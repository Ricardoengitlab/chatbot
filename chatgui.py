import nltk
from nltk.stem import WordNetLemmatizer
import pickle
import numpy as np
from keras.models import load_model
import json
import random
import spacy
from spacy import displacy
from collections import Counter
import en_core_web_sm
import es_core_news_sm
import tkinter
from tkinter import *
from actualiza import escribefruta
from actualiza import escribefrase

lemmatizer = WordNetLemmatizer()
model = load_model('chatbot_model.h5')                      #Corresponde a la red de intents.
modelFruits = load_model('chatbot_model2Fruits.h5')         #corresponde a la red de frutas.
context = [''] * 10                                         
nlp = es_core_news_sm.load()                                #Detecta entidades en español

intents = json.loads(open('intents.json').read())           #Para obteter informacion de los intents.
frutas = json.loads(open('fruits.json').read())             #Para obtener informacion de las calorias

words = pickle.load(open('words.pkl','rb'))                 #Palabras y tag de intents
classes = pickle.load(open('classes.pkl','rb'))

words2 = pickle.load(open('words2.pkl','rb'))               #Palabras y tipos de frutos
classes2 = pickle.load(open('classes2.pkl','rb'))


def clean_up_sentence(sentence):
    #lemmatiza una oracion
    
    sentence_words = nltk.word_tokenize(sentence)
    sentence_words = [lemmatizer.lemmatize(word.lower()) for word in sentence_words]
    return sentence_words


def bow(sentence, words, show_details=True):
    # coincidencias con las palabras de la oracion y las palabras
    
    sentence_words = clean_up_sentence(sentence)
    
    bag = [0]*len(words)
    for s in sentence_words:
        for i,w in enumerate(words):
            if w == s:
                bag[i] = 1
                if show_details:
                    print ("found in bag: %s" % w)
    return(np.array(bag))


def predict_class(sentence, model):
    #Calcula a que intents pertenece una oracion con una probabilidad y se establece un error de probabilidad.

    p = bow(sentence, words,show_details=False)
    res = model.predict(np.array([p]))[0]
    ERROR_THRESHOLD = 0.25
    results = [[i,r] for i,r in enumerate(res) if r>ERROR_THRESHOLD]
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
    return return_list


def predict_fruit(sentence, model):
    #Calcula a que fruta hace referecia con una probabilidad que esté por debajo de un error

    p = bow(sentence, words2,show_details=True)
    res = model.predict(np.array([p]))[0]
    ERROR_THRESHOLD = 0.25
    results = [[i,r] for i,r in enumerate(res) if r>ERROR_THRESHOLD]
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append({"item": classes2[r[0]], "probability": str(r[1])})
    return return_list


def getResponse(msg, doc, ints, intents_json):
    #El chatbot da una respuesta.

    global context
    tag = ints[0]['intent']
    list_of_intents = intents_json['intents']
    probability = float(ints[0]['probability'])
    print(ints)
    for i in list_of_intents:
        if(probability < .75 and context[0] != "preguntaFruta"):             #Si una oracion no es muy seguro que pertenezca a un intent
            result = f"(probabilidad: {probability})Estas solicitando {tag}?"
            context[0] = "intentCorrecto"
            context[2] = tag
            context[3] = msg
            break
        if(msg == "Si" and context[0] == "intentCorrecto"):                 #Si resulta que si pertenece al intent el chatbot aprende
            escribefrase("intents.json", context[2], context[3])
            context[0] = context[2]
            context[2] = ''
            context[3] = ''
        if(msg == "No" and context[0] == "intentCorrecto"):                 #Si resulta que no pertenece al intent el chatbot no aprende            context[0] = context[2]
            context[2] = ''
            context[3] = ''
            result = "Pregunta de nuevo"
            break
        try:
            if(tag == "nombre" or context[0] == "preguntaNombre"):          #Bienvenida al usuario
                result = f"Hola {doc[0][0]} ¿En qué puedo ayudarte?"
                context = ['']*10
                break
        except:
            pass
        if(context[0] == "aprende"):                                        #Aprende una nueva forma de escribir alguna fruta.
            if(msg == "Si"):
                escribefruta("fruits.json", context[2], context[3])
                result = f"Lo recordaré({context[3]})"
                context[0] = ''
                context[2] = ''
                context[3] = ''
                break
            else:
                context[0] = ''
                context[2] = ''
                context[3] = ''
                result = f"No lo recordaré {context[3]}"
                break
        if(context[0] == "preguntaFruta"):                                  #Predice la fruta que puede o no ser la buscada                                         
            fruits = predict_fruit(" ".join(list(msg)),modelFruits)
        
            for element in frutas['items']: 
                if(element['id'] == fruits[0]['item']):
                    result = fruits[0]['probability'] + " - " + element['name'][0].replace(" ", "") + " tiene " + element['quantity'] + " calorias. ¿Es este el resultado que buscabas?"
                    break
            context[0] = "aprende"  
            context[2] = element['id']
            context[3] = msg           
            break            
        if(context[0] == "preguntaEdad"):                                   #Respuesta para tomar agua
            result = "Toma al menos 2L de agua"
            context = ['']*10
            break
        if(context[0] == "preguntaEstado"):                                 #Respuesta para cuando el usuario responde estár bien
            result = "Me alegro mucho"
            context = ['']*10
            break
        if(i['tag']== tag):                                                 #Regresa la respuestas que no necesitan algún calculo.
            result = random.choice(i['responses'])
            print(i['tag'])
            context[0] = i['context']
            break
    print(context)
    return result


def chatbot_response(msg):
    #Obtiene los datos para que el chatbot de una respuesta

    doc = nlp(msg)
    doc = [[X.text, X.label_] for X in doc.ents]
    ints = predict_class(msg, model)
    res = getResponse(msg, doc, ints, intents)
    return res

def send():
    #Acciones que se hacen cuando se da click al boton SEND
    msg = EntryBox.get("1.0",'end-1c').strip()
    EntryBox.delete("0.0",END)
    
    if msg != '':
        ChatLog.config(state=NORMAL)
        ChatLog.insert(END, "You: " + msg + '\n\n')
        ChatLog.config(foreground="#442265", font=("Verdana", 12 ))

        res = chatbot_response(msg)
        ChatLog.insert(END, "Bot: " + res + '\n\n')

        ChatLog.config(state=DISABLED)
        ChatLog.yview(END)


base = Tk()
base.title("Hello")
base.geometry("400x700")
base.resizable(width=FALSE, height=TRUE)

#Create Chat window
ChatLog = Text(base, bd=0, bg="white", height="8", width="50", font="Arial",)

ChatLog.config(state=DISABLED)

#Bind scrollbar to Chat window
scrollbar = Scrollbar(base, command=ChatLog.yview, cursor="heart")
ChatLog['yscrollcommand'] = scrollbar.set

#Create Button to send message
SendButton = Button(base, font=("Verdana",12,'bold'), text="Send", width="12", height=5,
                    bd=0, bg="#32de97", activebackground="#3c9d9b",fg='#ffffff',
                    command=lambda:send() )

#Create the box to enter message
EntryBox = Text(base, bd=0, bg="white",width="29", height="5", font="Arial")
#EntryBox.bind("<Return>", send)


#Place all components on the screen
scrollbar.place(x=376,y=6, height=386)
ChatLog.place(x=6,y=6, height=586, width=370)
EntryBox.place(x=128, y=601, height=90, width=265)
SendButton.place(x=6, y=601, height=90)

base.mainloop()

if __name__ == "__main__":
    main()