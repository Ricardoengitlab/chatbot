import json
import os

def escribefruta(documento, id, texto):
    #Escribe una fruta en un documento
    
    filename = documento
    with open(filename, 'r') as f:
        data = json.load(f)
        data['items'][int(id)-1000]['name'].append(texto) # <--- add `id` value.
        data['items'][int(id)-1000]['name'].append(" ".join(list(texto))) # <--- add `id` value.

    os.remove(filename)
    with open(filename, 'w') as f:
        json.dump(data, f, indent=4,ensure_ascii=False)

def escribefrase(documento, tag, texto):
    #Escribe la frase a un documento

    filename = documento
    with open(filename, 'r') as f:
        data = json.load(f)
        for i in data["intents"]: # <--- add `id` value.
            if(i['tag'] == tag):
                i['patterns'].append(texto)

    os.remove(filename)
    with open(filename, 'w') as f:
        json.dump(data, f, indent=4,ensure_ascii=False)